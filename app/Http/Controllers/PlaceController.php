<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Place;

class PlaceController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $places = Place::all();

        return response()->json($places, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request) {
        if($request->has('name')) {
            $place          = new Place();
            $place->name    = $request->input('name');
            $place->appKey  = str_random(env('API_KEYS_LENGTH'));
            $place->enable  = 1;
            $place->save();

            $errorCode  = 200;
            $returned   = [
                'errorCode' => $errorCode,
                'success'   => true,
                'errorMsg'  => null,
                'appKey'    => $place->appKey,
            ];
        } else {
            $errorCode  = 400;
            $returned   = [
                'errorCode' => $errorCode,
                'success'   => false,
                'errorMsg'  => 'Missing parameter',
            ];
        }

        return response()->json($returned, $errorCode);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id) {
        try {
            $place  = Place::findOrFail($id);

            $errorCode  = 200;
            $returned   = $place;

        } catch (\Exception $e) {
            $errorCode  = 500;
            $returned   = [
                'errorCode' => $errorCode,
                'success'   => false,
                'errorMsg'  => 'Internal server error',
            ];
        }

        return response()->json($returned, $errorCode);
    }

    /**
     * @todo Open access to admins
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update() {
        $errorCode  = 500;
        $returned   = [
            'errorCode' => $errorCode,
            'success'   => false,
            'errorMsg'  => 'Access forbidden',
        ];

        return response()->json($returned, $errorCode);
    }

    /**
     * @todo Open access to admins
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy() {
        $errorCode  = 500;
        $returned   = [
            'errorCode' => $errorCode,
            'success'   => false,
            'errorMsg'  => 'Access forbidden',
        ];

        return response()->json($returned, $errorCode);
    }
}
