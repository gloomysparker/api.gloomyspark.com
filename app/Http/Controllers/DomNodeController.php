<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\DomNode;
use \App\Place;

class DomNodeController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @todo Open access to admins
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $errorCode  = 500;
        $returned   = [
            'errorCode' => $errorCode,
            'success'   => false,
            'errorMsg'  => 'Access forbidden',
        ];

        return response()->json($returned, $errorCode);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request) {
        if(
            $request->has('placeId') AND
            $request->has('name') AND
            $request->has('macAddress')
        ) {
            $placeId = $request->input('placeId');

            try {
                $place = Place::findOrFail($placeId);

                $domNode                = new DomNode();
                $domNode->name          = $request->input('name');
                $domNode->macAddress    = $request->input('macAddress');
                $domNode->placeId       = $place->id;
                $domNode->appSecret     = str_random(env('API_KEYS_LENGTH'));
                $domNode->enable        = 1;
                $domNode->save();

                $errorCode  = 200;
                $returned   = [
                    'errorCode' => $errorCode,
                    'success'   => true,
                    'errorMsg'  => null,
                    'appSecret' => $domNode->appSecret,
                ];
            } catch (\Exception $e) {
                $errorCode  = 500;
                $returned   = [
                    'errorCode' => $errorCode,
                    'success'   => false,
                    'errorMsg'  => 'Internal server error',
                ];
            }
        } else {
            $errorCode  = 400;
            $returned   = [
                'errorCode' => $errorCode,
                'success'   => false,
                'errorMsg'  => 'Missing parameter',
            ];
        }

        return response()->json($returned, $errorCode);
    }

    /**
     * @todo Open access to admins
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id) {
        $errorCode  = 500;
        $returned   = [
            'errorCode' => $errorCode,
            'success'   => false,
            'errorMsg'  => 'Access forbidden',
        ];

        return response()->json($returned, $errorCode);
    }

    /**
     * @todo Open access to admins
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update() {
        $errorCode  = 500;
        $returned   = [
            'errorCode' => $errorCode,
            'success'   => false,
            'errorMsg'  => 'Access forbidden',
        ];

        return response()->json($returned, $errorCode);
    }

    /**
     * @todo Open access to admins
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy() {
        $errorCode  = 500;
        $returned   = [
            'errorCode' => $errorCode,
            'success'   => false,
            'errorMsg'  => 'Access forbidden',
        ];

        return response()->json($returned, $errorCode);
    }
}
