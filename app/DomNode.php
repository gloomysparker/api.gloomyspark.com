<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomNode extends Model
{
    protected $hidden = [
        'macAddress',
        'appSecret',
        ];

    public function place() {
        return $this->belongsTo('\App\Place', 'placeId');
    }
}
