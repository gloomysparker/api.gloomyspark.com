<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $hidden = ['appKey',];

    public function domNodes() {
        return $this->hasMany('\App\DomNode', 'placeId');
    }
}
