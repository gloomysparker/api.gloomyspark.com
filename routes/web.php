<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource(
    'domnode',
    'DomNodeController',
    [
        'only' => [
            'index',
            'store',
            'show',
            'update',
            'destroy',
            ]
    ]);

Route::resource(
    'place',
    'PlaceController',
    [
        'only' => [
            'index',
            'store',
            'show',
            'update',
            'destroy',
        ]
    ]);
