<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dom_nodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('macAddress');
            $table->string('placeId');
            $table->string('appSecret');
            $table->boolean('enable')->default(2);
            $table->timestamps();

            $table->index([
                'name',
                'macAddress',
                'appSecret',
                'enable',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dom_nodes');
    }
}
