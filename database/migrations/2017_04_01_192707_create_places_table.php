<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('appKey');
            $table->boolean('enable')->default(2);
            $table->timestamps();

            $table->unique([
                'appKey',
            ]);
            $table->index([
                'name',
                'enable',
            ]);
        });

        /** INSERT DEFAULT HOME PLACE */
        DB::table('places')->insert(
            [
                'name'      => 'Home Sweet Home',
                'appKey'    => env('API_APP_KEY'),
                'enable'    => 1,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
